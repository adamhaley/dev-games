var gulp = require("gulp");
var sourcemaps = require("gulp-sourcemaps");
var babel = require("gulp-babel");
var concat = require("gulp-concat");
var browserSync = require('browser-sync').create();



// gulp.task("babel", function() {
// 	return gulp.src("main.es")
//     .pipe(sourcemaps.init())
//     .pipe(babel())
//     .pipe(concat("main.js"))
//     .pipe(sourcemaps.write("."))
//     .pipe(gulp.dest("."));
// });

// Static server
gulp.task("browser-sync", function() {
    browserSync.init({
        proxy: "http://localhost/devgames/"
    });

    gulp.watch("index.html").on("change", browserSync.reload);
    // gulp.watch("main.es").on("change", ["babel"]);
    gulp.watch("main.js").on("change", browserSync.reload);
});


// gulp.task("default", ["babel","browser-sync"]);